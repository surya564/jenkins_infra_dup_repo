#!groovy
import jenkins.model.Jenkins
import hudson.model.ListView

Jenkins jenkins = Jenkins.getInstance()

def custom_log_location= "/var/lib/jenkins/logs/custom_log.txt";
File file = new File(custom_log_location)
file.append("--------------------------------------------------------------- \n")
file.append("Executing /var/lib/jenkins/init.groovy.d/create_views.groovy \n")

def viewName = 'MyView'

// get the view
getViewName = hudson.model.Hudson.instance.getView(viewName)

if (getViewName == null) {
  // Create the view if it doesn't exist or if no existing job has the same name
  jenkins.addView(new ListView(viewName))
}

file.append("==================================================================================================== \n")
file.append("Successfully created testView\n")
file.append("==================================================================================================== \n\n\n")

jenkins.save()
