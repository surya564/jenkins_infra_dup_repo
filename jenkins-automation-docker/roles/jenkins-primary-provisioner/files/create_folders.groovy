import com.cloudbees.hudson.plugins.folder.*
import org.jenkinsci.plugins.workflow.job.WorkflowJob
import jenkins.model.Jenkins


def custom_log_location= "/var/lib/jenkins/logs/custom_log.txt";
File file = new File(custom_log_location)
file.append("--------------------------------------------------------------- \n")
file.append("Executing /var/lib/jenkins/init.groovy.d/create_folders.groovy \n")


Jenkins jenkins = Jenkins.instance // saves some typing
/*
// Bring some values in from ansible using the jenkins_script modules wierd "args" approach (these are not gstrings)
String folderName = "Credentials"

def folder = jenkins.getItem(folderName)
if (folder == null) {
  // Create the folder if it doesn't exist or if no existing job has the same name
  folder = jenkins.createProject(Folder.class, folderName)
} else {
  if (folder.getClass() != Folder.class) {
    // when folderName exists, but is not a folder we make the folder with a temp name
    folder = jenkins.createProject(Folder.class, folderNameTemp)
    // Move existing jobs from the same environment to folders (preseve history)
    //Item[] items = jenkins.getItems(WorkflowJob.class)

    Item[] items = ['Test Credential ADCAutomatedBuild','Test Credentials (x-adc-rd-devops user on bitbucket.devops.abbott)','Test Credentials (x-adc-rd-devops read)']
    def job_regex = "^" + folderName

    items.grep { it.name =~ job_regex }.each { job ->
      Items.move(job, folder)
    }

    // Rename the temp folder now we've moved the jobs
    folder.renameTo(folderName)
  }
}
*/

String folderName = "Admin"

def folder = jenkins.getItem(folderName)
if (folder == null) {
  // Create the folder if it doesn't exist or if no existing job has the same name
  folder = jenkins.createProject(Folder.class, folderName)
  def items = ["test_cred_ADCAutomatedBuild","test_cred_x-adc-rd-devops read","test_cred_x-adc-rd-devops user on bitbucket.devops.abbott"]
	  items.each { job ->
      movingJob = jenkins.getItem(job)
      Items.move(movingJob, folder)
    }
}

file.append("==================================================================================================== \n")
file.append("Successfully created testFolders \n")
file.append("==================================================================================================== \n\n\n")

jenkins.save()
