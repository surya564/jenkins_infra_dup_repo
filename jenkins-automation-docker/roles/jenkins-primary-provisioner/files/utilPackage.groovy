package utilPackage

import jenkins.model.*
import groovy.json.*
import java.util.logging.Logger

class countClass {

    //Start of roleCount ----------------------------------------------------
     def static roleCount() {
         def PERM_JSON_FILE = "/var/lib/jenkins/init.groovy.d/roles.json"
         if ( "${PERM_JSON_FILE}" != "")  {
           File f = new File(PERM_JSON_FILE)
           def jsonSlurper = new JsonSlurper()
           def jsonText = f.getText()
           def roleparse = jsonSlurper.parseText( jsonText )

           def totalRoles = 0
             roleparse.each {
               totalRoles =totalRoles +1
             }
           return totalRoles
           }
         }
     //End of roleCount -------------------------------------------------------

     //Start of pluginCountMethod ---------------------------------------------------------------
     def static pluginCount() {
       def filepath="/var/lib/jenkins/init.groovy.d/pluginslist.cfg";
       def pluginCount = 0
       if(new File( filepath).exists()){
           new File( filepath ).eachLine {
             pluginCount = pluginCount + 1
           }
         return pluginCount
       }
     }
   	 //Start of pluginCountMethod ---------------------------------------------------------------

      //Start of userCountMethod ---------------------------------------------------------------
      def static userCountMethod() {
        def filepath="/var/lib/jenkins/init.groovy.d/userlist.cfg";
        def userlist = []
        def userCount = 0
        if(new File( filepath).exists()){
            new File( filepath ).eachLine { line ->
                userlist << line
            }
            if(userlist.size()>0){
            userlist.each {
                userCount = userCount + 1
            }
            }
          return userCount
        }
      }
      //End of userCountMethod ---------------------------------------------------------------

    //Start of permCountMethod -----------------------------------------------------------------
    //Permissions for each role will be returned through this method based on the role passed.
    def static permCountMethod(String rolename) {
        def PERM_JSON_FILE = "/var/lib/jenkins/init.groovy.d/permissions.json"
        if ( "${PERM_JSON_FILE}" != "")  {
          File f = new File(PERM_JSON_FILE)
          def jsonSlurper = new JsonSlurper()
          def jsonText = f.getText()
          def accessperm = jsonSlurper.parseText( jsonText )

          def permCount = 0
          accessperm."${rolename}".each {
              permCount =permCount +1
            }
          return permCount
          }
        }
    //End of permCountMethod -------------------------------------------------------------------

    //Start of userCountPerRole-----------------------------------------------------------------
    //
    def static userCountPerRole(String rolename) {
        def AUTHZ_JSON_FILE = "/var/lib/jenkins/init.groovy.d/roles.json"
        if ( "${AUTHZ_JSON_FILE}" != "")  {
          File f = new File(AUTHZ_JSON_FILE)
          def jsonSlurper = new JsonSlurper()
          def jsonText = f.getText()
          def access = jsonSlurper.parseText( jsonText )

          def userCount = 0
              access."${rolename}".each {
              userCount =userCount +1
            }
          return userCount
        }
      }
    //End of userCountPerRole ------------------------------------------------------------------

}
