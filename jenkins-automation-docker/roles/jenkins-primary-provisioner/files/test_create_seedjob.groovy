import com.cloudbees.hudson.plugins.folder.*
import hudson.plugins.git.*
import jenkins.*
import jenkins.model.*
import hudson.*
import javaposse.jobdsl.plugin.*
import hudson.model.*
def jenkinsInstance = Jenkins.getInstance()

String folderName = "Infra Test Jobs"
def folder = jenkinsInstance.getItem(folderName)
if (folder == null) {

def jobName = "Seed Job for Testing Credentials"
def seedProject = new FreeStyleProject(jenkinsInstance, jobName);

seedProject.save()

def jobDslBuildStep = new ExecuteDslScripts()
jobDslBuildStep.with {
    ignoreExisting = true
    lookupStrategy = LookupStrategy.JENKINS_ROOT
    removedJobAction = RemovedJobAction.DELETE
    removedViewAction = RemovedViewAction.DELETE
    useScriptText = true
    scriptText = new File('/var/lib/jenkins/init.groovy.d/test_jobs.txt').getText('UTF-8')
}
seedProject.getBuildersList().add(jobDslBuildStep)

jenkinsInstance.reload()

  folder = Jenkins.instance.createProject(Folder.class, folderName)
  def item = "Seed Job for Testing Credentials"
      movingJob = jenkinsInstance.getItem(item)
      Items.move(movingJob, folder)
}

jenkinsInstance.save()
