import hudson.model.*
import jenkins.model.*
import jenkins.security.*
import jenkins.security.apitoken.*

// script parameters
def userName = 'devops'
def tokenName = 'agent_creation'

def user = User.get(userName, false)
def apiTokenProperty = user.getProperty(ApiTokenProperty.class)
def result = apiTokenProperty.tokenStore.generateNewToken(tokenName)
user.save()

//return result.plainValue

def custom_log_location= "/var/lib/jenkins/apitoken.txt";
File file = new File(custom_log_location)
file.createNewFile()
file.write("agent_creation:" + result.plainValue)
