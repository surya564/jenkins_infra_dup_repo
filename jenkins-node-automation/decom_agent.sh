#decom_agent.sh
#Disconnect and Delete an agent connected to the Jenkins Primary
#To run this script sh decom_agent.sh <nodename> <username> <userpassword>

p_node_name=$1
p_username=$2
p_password=$3
SERVER="https://jenkins2.freestyleserver.com"
COOKIEJAR="$(mktemp)"
CRUMB=$(curl -f -u $p_username:$p_password --cookie-jar "$COOKIEJAR" "$SERVER/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,%22:%22,//crumb)")

curl -f -X POST -u $p_username:$p_password --cookie "$COOKIEJAR" -H "$CRUMB" -H "Content-Length: 0" "$SERVER/computer/${p_node_name}/doDisconnect?offlineMessage=bye"
curl -f -X POST -u $p_username:$p_password --cookie "$COOKIEJAR" -H "$CRUMB" -H "Content-Length: 0" "$SERVER/computer/${p_node_name}/doDelete?offlineMessage=bye"
