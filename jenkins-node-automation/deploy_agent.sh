#deploy_agent.sh
#Launch an agent and connect it to the Jenkins Primary
#To run this script sh deploy_agent.sh <nodename> <username> <userpassword>

p_node_name=$1
p_username=$2
p_password=$3
p_jenkins_url="https://jenkins2.freestyleserver.com"
COOKIEJAR="$(mktemp)"
CRUMB=$(curl -f -u $p_username:$p_password --cookie-jar "$COOKIEJAR" "$p_jenkins_url/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,%22:%22,//crumb)")

JSON_OBJECT="{   'name':+'$p_node_name',
      +'nodeDescription':+'testnode agent',
        +'numExecutors':'1',
          +'remoteFS':+'/Users/informatics/jenkins',
            +'labelString':+'',
              +'mode':+'EXCLUSIVE',
              +'':+['hudson.slaves.JNLPLauncher',+'hudson.slaves.RetentionStrategy\$Always'],
              +'launcher':+{'stapler-class':+'hudson.slaves.JNLPLauncher',+'\$class':+'hudson.slaves.JNLPLauncher',
                +'workDirSettings':+{'disabled':+'',+'workDirPath':+'',+'internalDir':+'remoting',+'failIfWorkDirIsMissing':+false},
              +'tunnel':+''},
              +'retentionStrategy':+{'stapler-class':+'hudson.slaves.RetentionStrategy\$Always',
                                      +'\$class':+'hudson.slaves.RetentionStrategy\$Always'},
              +'nodeProperties':+{'stapler-class-bag':+'true',
                                  +'hudson-slaves-EnvironmentVariablesNodeProperty':+{'env':+[{'key':+'ANDROID_HOME',+'value':+'/Users/Informatics/Library/Android/sdk'},
                                                                                             +{'key':+'ANDROID_SDK_ROOT',+'value':+'/usr/local/share/android-sdk'},
                                                                                             +{'key':+'PATH+ANDROID_EMULATOR',+'value':+'ANDROID_HOME/emulator'},
                                                                                             +{'key':+'PATH+ANDROID_TOOLS',+'value':+'ANDROID_HOME/tools'},
                                                                                             +{'key':+'PATH+PLATFORM_TOOLS',+'value':+'ANDROID_HOME/platform-tools'}]}
                                                                                                                        }}"

RESPONSE=$(curl -L -s -o /dev/null -v -k -w "%{http_code}" -u $p_username:$p_password --cookie "$COOKIEJAR" -H "Content-Type:application/x-www-form-urlencoded" -H "$CRUMB" -X POST -d "json=${JSON_OBJECT}" "${p_jenkins_url}/computer/doCreateItem?name=${p_node_name}&type=hudson.slaves.DumbSlave")

SECRET=$(curl -L -s -u $p_username:$p_password -H $CRUMB $p_jenkins_url/computer/${p_node_name}/slave-agent.jnlp | sed "s/.*<application-desc main-class=\"hudson.remoting.jnlp.Main\"><argument>\([a-z0-9]*\).*/\1/")
java -jar agent.jar -jnlpUrl $p_jenkins_url/computer/${p_node_name}/slave-agent.jnlp -secret $SECRET -workDir "/Users/informatics/jenkins"
