Set-TimeZone "Coordinated Universal Time"

# Install Boxstarter
. { iwr -useb https://boxstarter.org/bootstrapper.ps1 } | iex; Get-Boxstarter -Force

# Copy setup.ps1 to the Temp directory and then run boxstarter with our setup.ps1 script
$env:PSModulePath = "$([System.Environment]::GetEnvironmentVariable('PSModulePath', 'User'));$([System.Environment]::GetEnvironmentVariable('PSModulePath', 'Machine'))"
cp /Users/BALLASX7/Documents/ansible_project/jenkins-automation/roles/jenkins-win-agent-provisioner/files/setup.ps1 $env:TEMP
Import-Module Boxstarter.Chocolatey
$credential = New-Object System.Management.Automation.PSCredential("vagrant", (ConvertTo-SecureString "vagrant" -AsPlainText -Force))
Install-BoxstarterPackage $env:TEMP\setup.ps1 -Credential $credential

$Username = "devops"
$Password = "Jenkins123"

$group = "Administrators"

$adsi = [ADSI]"WinNT://$env:COMPUTERNAME"
$existing = $adsi.Children | where {$_.SchemaClassName -eq 'user' -and $_.Name -eq $Username }

if ($existing -eq $null) {

    Write-Host "Creating new local user $Username."
    & NET USER $Username $Password /add /y /expires:never

    Write-Host "Adding local user $Username to $group."
    & NET LOCALGROUP $group $Username /add

}
else {
    Write-Host "Setting password for existing local user $Username."
    $existing.SetPassword($Password)
}

Write-Host "Ensuring password for $Username never expires."
& WMIC USERACCOUNT WHERE "Name='$Username'" SET PasswordExpires=FALSE


"Jenkins123" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Out-File "C:\tmp\Password.txt"

# $username1 = “devops”
$password1 = cat C:\tmp\Password.txt | convertto-securestring
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $Username, $password1
# Start-Process powershell.exe -Credential $cred -ArgumentList "runas /user:devops cmd"
start-process -FilePath "C:\WINDOWS\system32\cmd.exe" -Credential $cred

$content = Get-Content -Path 'C:\Program Files\OpenSSH-Win64\sshd_config_default'
$newContent = $content -replace '#PubkeyAuthentication yes','PubkeyAuthentication yes'
$newContent | Set-Content -Path 'C:\Program Files\OpenSSH-Win64\sshd_config_default'
Get-Content -path "C:\Program Files\OpenSSH-Win64\sshd_config_default"

New-Item -Path 'C:\Users\devops\.ssh' -ItemType Directory
New-Item C:\Users\devops\.ssh\authorized_keys

New-Item -Path 'C:\jenkinsnode' -ItemType Directory

 $Packages = 'openjdk8', 'git', 'vim'
   ForEach ($PackageName in $Packages)
   {
       choco install $PackageName -y
   }
