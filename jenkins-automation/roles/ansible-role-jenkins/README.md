Role Name
=========
ansible-role-jenkins is used to provision a Jenkins environment.

Requirements
------------
This infrastructure requires vagrant and virtual box installed on the localhost.

Instructions:
------------
With vagrant and virtual box setup: Run 'vagrant up' in /adc-devops-jenkins-infra folder.
Jenkins will run on http://10.0.15.15:8080/

Role Variables
--------------
A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------
Plugins are going to listed at roles/ansible-role-jenkins/files/pluginslist.cfg

Example Playbook
----------------
How to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Install Jenkins
      hosts: localhost
      roles:
        - role: ansible-role-jenkins

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
