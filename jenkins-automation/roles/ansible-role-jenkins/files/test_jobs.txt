job('Infra Test Jobs/Test Credential ADCAutomatedBuild') {
  scm {
    git {
        remote {
            url('https://adcautomatedbuild@git.adc.abbott.collab.net/gerrit/librelinkadcios')
            credentials('124d55b6-4ee6-44f6-83b1-16e821bb4477')
        }
      branch('*/master')
      }
    }
}
job('Infra Test Jobs/Test Credentials (x-adc-rd-devops read)') {
  scm {
    git {
        remote {
            url('ssh://git@bitbucket-ssh.devops.abbott:7999/adc_n/adc-nav-ios-app-core.git')
            credentials('x-adc-rd-devops.bitbucket.devops.abbott')
        }
      branch('*/master')
      }
    }
}
job('Infra Test Jobs/Test Credentials (x-adc-rd-devops user on bitbucket.devops.abbott)') {
  scm {
    git {
        remote {
            url('ssh://git@bitbucket-ssh.devops.abbott:7999/adc_1203/adc-clinical-fsll-ios-app.git')
            credentials('x-adc-rd-devops.bitbucket.devops.abbott-read')
        }
      branch('*/master')
      }
    }
}
