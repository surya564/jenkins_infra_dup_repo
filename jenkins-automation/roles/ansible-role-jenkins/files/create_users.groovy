import hudson.model.*
import hudson.security.*
import hudson.tasks.Mailer

def custom_log_location= "/var/lib/jenkins/logs/custom_log.txt";
File file = new File(custom_log_location)
file.append("--------------------------------------------------------------- \n")
file.append("Executing /var/lib/jenkins/init.groovy.d/create_users.groovy \n")
def userCount = 0

def fileSeperator=File.separator;
println("before creating users")
def filepath="/var/lib/jenkins/init.groovy.d"+fileSeperator+"userlist.cfg";
println "userlist.cfg FilePath : $filepath"
def userlist = []
def instance = jenkins.model.Jenkins.instance

if(new File( filepath).exists()){
    new File( filepath ).eachLine { line ->
        userlist << line
    }
	   if(userlist.size()>0){
	    userlist.each {

      String userstr = it;
      String[] str;
      str = userstr.split(',');

      String[] variables = it.split(',',4);
          String userId = variables[0];
  		    String fullname = variables[1];
  		    String email = variables[2];
        	String password = variables[3];

          println(userId+","+password+","+email+","+fullname)

        def user = instance.securityRealm.createAccount(userId, password)
        userCount = userCount + 1
        user.addProperty(new Mailer.UserProperty(email));
        user.setFullName(fullname)
        instance.save()
        }
    }
}

def customUtilClass = this.class.classLoader.parseClass(new File("/var/lib/jenkins/init.groovy.d/utilPackage.groovy"))
def totalUserCount = customUtilClass.userCountMethod()

file.append("==================================================================================================== \n")
file.append("Total no.of User expected to be created is "+totalUserCount+"." + " Setup successfully completed for " + userCount + "\n")
file.append("==================================================================================================== \n\n\n")
