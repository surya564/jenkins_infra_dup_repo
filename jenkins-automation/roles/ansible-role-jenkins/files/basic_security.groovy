#!groovy
import java.util.*
import java.lang.reflect.*
import java.util.logging.*
import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.security.*
import jenkins.install.InstallState
import com.michelin.cio.hudson.plugins.rolestrategy.*
import com.synopsys.arc.jenkins.plugins.rolestrategy.*
import com.synopsys.arc.jenkins.plugins.rolestrategy.RoleType;
import groovy.json.*

def instance = Jenkins.getInstance()

def custom_log_location= "/var/lib/jenkins/logs/custom_log.txt";
File file = new File(custom_log_location)
file.createNewFile()
file.write("--------------------------------------------------------------- \n")
file.append("Executing /var/lib/jenkins/init.groovy.d/basic_security.groovy \n")
def roles_created = 0

// Change authorization strategy to RoleBasedAuthorizationStrategy
def roleBasedAuthenticationStrategy = new RoleBasedAuthorizationStrategy()
instance.setAuthorizationStrategy(roleBasedAuthenticationStrategy)

file.append("================================================================ \n")
file.append("Authorization Strategy now set to RoleBasedAuthorizationStrategy \n")
file.append("================================================================ \n\n\n")

// Get Permissions from roles.json
def AUTHZ_JSON_FILE = "/var/lib/jenkins/init.groovy.d/roles.json"
if ( "${AUTHZ_JSON_FILE}" != "")  {
  println "Get role authorizations from file ${AUTHZ_JSON_FILE}"
  File f = new File(AUTHZ_JSON_FILE)
  def jsonSlurper = new JsonSlurper()
  def jsonText = f.getText()
  access = jsonSlurper.parseText( jsonText )
}

// Get Permissions from permissions.json
def PERM_JSON_FILE = "/var/lib/jenkins/init.groovy.d/permissions.json"
if ( "${PERM_JSON_FILE}" != "")  {
  println "Get role authorizations from file ${PERM_JSON_FILE}"
  File f = new File(PERM_JSON_FILE)
  def jsonSlurper = new JsonSlurper()
  def jsonText = f.getText()
  accessperm = jsonSlurper.parseText( jsonText )
}

file.append("--------------------------------------------------------------- \n")
file.append "Creating Roles \n"

//------------------------------------------------------------------------------------------------------------------------------------------------
// Create Global admin role
def globalRoleAdmin = "admin"

// Create admin role set of permissions
Set<Permission> adminPermissions = new HashSet<Permission>();
accessperm.admins.each { l ->
  adminPermissions.add(Permission.fromId(l));
}

// Create Role
Role adminRole = new Role(globalRoleAdmin, adminPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), adminRole);


access.admins.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), adminRole, l);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

file.append("Admin role setup complete \n")
roles_created = roles_created+1

//------------------------------------------------------------------------------------------------------------------------------------------------
// Create Global Developer role
def globalRoleDeveloper = "Developer"

// Create Developer role set of permissions
Set<Permission> developerPermissions = new HashSet<Permission>();
accessperm.Developer.each { l ->
  developerPermissions.add(Permission.fromId(l));
}

// Create the Developer Role
Role developerRole = new Role(globalRoleDeveloper, developerPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), developerRole);

//Assign Developer Project/Item roles
access.Developer.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), developerRole, l);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

file.append("Developer role setup complete \n")
roles_created = roles_created+1

//------------------------------------------------------------------------------------------------------------------------------------------------
// Create Global NAV role
def globalRoleNAV = "NAV"

// Create NAV role set of permissions
Set<Permission> NAVPermissions = new HashSet<Permission>();
accessperm.NAVGlobal.each { l ->
    NAVPermissions.add(Permission.fromId(l));
}

// Create the NAV Role
Role NAVRole = new Role(globalRoleNAV, NAVPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), NAVRole);

access.NAVGlobal.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), NAVRole, l);
}

//------------------------------------------------------------------------------
// Create project/item NAV role
def projectRoleNAV = "NAV"
Set<Permission> NAVprojectPermissions = new HashSet<Permission>();
accessperm.NAVProject.each { l ->
  NAVprojectPermissions.add(Permission.fromId(l));
}

//Create the NAV Role
Role NAVRoleProject = new Role(projectRoleNAV, "(?i)NAV.*",NAVprojectPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.PROJECT), NAVRoleProject);

//Assign NAV Project/Item roles
access.NAVProject.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.PROJECT), NAVRoleProject, l);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

file.append("Global and Project NAV role setup complete \n")
roles_created = roles_created+2

//------------------------------------------------------------------------------------------------------------------------------------------------
// Create Global SAS-iSAS role
def globalRoleSAS = "SAS-iSAS"
// Create SAS role set of permissions
Set<Permission> SASPermissions = new HashSet<Permission>();
accessperm.SASGlobal.each { l ->
  SASPermissions.add(Permission.fromId(l));
}

// Create the SAS Role
Role SASRole = new Role(globalRoleSAS, SASPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), SASRole);

access.SASGlobal.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), SASRole, l);
}
//------------------------------------------------------------------------------
// Create project/item SAS-iSAS role
def projectRoleSAS = "SAS-iSAS"

Set<Permission> SASprojectPermissions = new HashSet<Permission>();
accessperm.SASProject.each { l ->
  SASprojectPermissions.add(Permission.fromId(l));
}

Role SASRoleProject = new Role(projectRoleSAS, "(?i)SAS-iSAS.*",SASprojectPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.PROJECT), SASRoleProject);

//Assign SAS Project/Item roles
access.SASProject.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.PROJECT), SASRoleProject, l);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

file.append("Global and Project SAS-iSAS role setup complete \n")
roles_created = roles_created+2

//------------------------------------------------------------------------------------------------------------------------------------------------
// Create global Trident_Android role
def globalRoleTA = "Trident_Android"

// Create NAV role set of permissions
Set<Permission> TAPermissions = new HashSet<Permission>();
accessperm.TAGlobal.each { l ->
  TAPermissions.add(Permission.fromId(l));
}

Role TARole = new Role(globalRoleTA, TAPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), TARole);

access.TAGlobal.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.GLOBAL), TARole, l);
}
//------------------------------------------------------------------------------
def projectRoleTA = "Trident_Android"

Set<Permission> TAprojectPermissions = new HashSet<Permission>();
accessperm.TAProject.each { l ->
  TAprojectPermissions.add(Permission.fromId(l));
}

// Create the NAV Role
Role TARoleProject = new Role(projectRoleTA, "(?i)ll_trident_android_.*",TAprojectPermissions);
roleBasedAuthenticationStrategy.addRole(RoleType.fromString(RoleBasedAuthorizationStrategy.PROJECT), TARoleProject);

access.TAProject.each { l ->
  roleBasedAuthenticationStrategy.assignRole(RoleType.fromString(RoleBasedAuthorizationStrategy.PROJECT), TARoleProject, l);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

file.append("Global and Project Trident_Android role setup complete \n")
roles_created = roles_created+2

def customUtilClass = this.class.classLoader.parseClass(new File("/var/lib/jenkins/init.groovy.d/utilPackage.groovy"))
def utilRoleCount = customUtilClass.roleCount()

file.append("==================================================================================================== \n")
file.append("Total no.of roles expected to setup is " + utilRoleCount + "." + " Setup successfully completed for " + roles_created + "\n")
file.append("==================================================================================================== \n\n\n")

instance.save()
