#!/bin/sh

#ssh -o StrictHostKeyChecking=no devops@10.0.15.18

ssh-keyscan -H 10.0.15.18 >> ~/.ssh/known_hosts

cd /home/devops/.ssh/

lftp<<END_SCRIPT
open sftp://devops:Jenkins123@10.0.15.18
cd .ssh
put authorized_keys
bye
END_SCRIPT
