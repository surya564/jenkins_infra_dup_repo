import com.cloudbees.hudson.plugins.folder.*
import hudson.plugins.git.*
import jenkins.*
import jenkins.model.*
import hudson.*
import javaposse.jobdsl.plugin.*
import hudson.model.*
def jenkinsInstance = Jenkins.getInstance()

def custom_log_location= "/var/lib/jenkins/logs/custom_log.txt";
File file = new File(custom_log_location)
file.append("--------------------------------------------------------------- \n")
file.append("Executing /var/lib/jenkins/init.groovy.d/create_seedjob.groovy \n")

def jobName = "Seed Job for Testing Credentials"
// jobDsl plugin uses a free style project in order to seed jobs, initialising it.
def seedProject = new FreeStyleProject(jenkinsInstance, jobName);

seedProject.save()

def jobDslBuildStep = new ExecuteDslScripts()
jobDslBuildStep.with {
    ignoreExisting = true
    lookupStrategy = LookupStrategy.JENKINS_ROOT
    removedJobAction = RemovedJobAction.DELETE
    removedViewAction = RemovedViewAction.DELETE
    useScriptText = true
    scriptText = new File('/var/lib/jenkins/init.groovy.d/test_jobs.txt').getText('UTF-8')
}
seedProject.getBuildersList().add(jobDslBuildStep)
jenkinsInstance.reload()

String folderName = "Infra Test Jobs"
def folder = jenkinsInstance.getItem(folderName)
if (folder == null) {
  // Create the folder if it doesn't exist or if no existing job has the same name
  folder = Jenkins.instance.createProject(Folder.class, folderName)
  def item = "Seed Job for Testing Credentials"
      movingJob = jenkinsInstance.getItem(item)
      Items.move(movingJob, folder)
}

file.append("==================================================================================================== \n")
file.append("Creation of Seed job is complete. \n")
file.append("==================================================================================================== \n\n\n")

jenkinsInstance.save()
