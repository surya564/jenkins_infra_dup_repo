Installing the Chart:
helm install {deployment_name} --set image.registry={registry_name} --set image.repository={repository/image_name} --set image.tag={image_tag} ./jenkins-primary-helm-chart/ --namespace {kube_namespace}

Uninstalling the Chart:
helm delete my-release

Full Readme file can be accessed by using below link:
https://github.com/bitnami/charts/tree/master/bitnami/jenkins
